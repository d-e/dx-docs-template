.. dx-punch documentation master file, created by
   sphinx-quickstart on Tue Dec 18 10:46:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to demo documentation for `dx` projects with Sphinx!
============================================================

A template configuration for generating sphinx docs
---------------------------------------------------

Features
^^^^^^^^

* Separate ``source`` and ``build`` directories.
* Expanded ``Makefile`` that generates automatically API docs
  based on local packages.

Usage
-----
.. toctree::
   chapters/usage

.. toctree::
   :hidden:
   :glob:

   _modules/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
