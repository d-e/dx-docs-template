********
API docs
********

.. note::
   
   Modify this `source`_ file with the following content::
      
      ..include:: <package.rst>

   where ``<package.rst>`` is generated through::

      $ make MODPATH=path/to/package apidoc

.. _source: ../_sources/_modules/modules.rst.txt

.. ..include:: <package>.rst
